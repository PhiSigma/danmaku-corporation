using System;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyPlayerDetection : CircleSegmentBehavior
{
    public float detectionRadius = 5;
    public float detectionEndRadius = 7;
    public float spread = 200;

    private Enemy _enemy;
    private Character _target;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
        _enemy.TargetChangeEvent += OnTargetChange;
    }

    private void OnTargetChange(object sender, EventArgsWithCharacter args)
    {
        if (_target) _target.DeathEvent -= OnTargetDeath;
        if (args.Character) args.Character.DeathEvent += OnTargetDeath;

        _target = args.Character;
    }

    private void OnTargetDeath(object sender, EventArgs args)
    {
        _enemy.Target = null;
    }

    private void Update()
    {
        if (!_enemy.IsControllable()) return;

        if (!_enemy.Target || 
            _enemy.Target && (_enemy.Target.transform.position - transform.position).magnitude > detectionEndRadius)
        {
            _enemy.Target = null;
        }

        if (_enemy.Target) return;
        
        foreach (PlayerCharacter player in ControllerManager.PlayerCharacters)
        {
            if (!player ||
                (player.transform.position - transform.position).magnitude > detectionRadius
                || !IsInSegment(player.transform.position))
            {
                continue;
            }
            
            _enemy.Target = player;
            return;
        }
    }

    private bool IsInSegment(Vector3 position)
    {
        if (spread > 180) return true;
        
        Vector3 transformed = transform.InverseTransformPoint(position);
        float angle = Mathf.Acos(transformed.z / transformed.magnitude) * 180 / Mathf.PI;
        
        return angle < spread;
    }

    
    public override float GetRange()
    {
        return detectionEndRadius > 100 ? detectionRadius : detectionEndRadius;
    }

    public override float GetSubRange()
    {
        return detectionEndRadius > 100 ? -1 : detectionRadius;
    }

    public override float GetSpread()
    {
        return spread;
    }
}