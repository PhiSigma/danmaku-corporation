using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[Serializable]
public struct ScoreInfo
{
    public float score;
    public string time;
}

[RequireComponent(typeof(Text))]
public class ScoreDisplay : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnChangeInfo))] private ScoreInfo _info;
    [SyncVar(hook = nameof(OnChangeVisible))] private bool _visible = true;
    
    private Text _text;
    private int _index;

    private void Start()
    {
        _text = GetComponent<Text>();
        _index = transform.GetSiblingIndex();

        if (!isServer)
        {
            OnChangeInfo(_info);
            OnChangeVisible(_visible);
            return;
        }
        
        OnGameOver(this, new EventArgs());
        
        Game.GameOverEvent += OnGameOver;
    }

    private void OnGameOver(object sender, EventArgs e)
    {
        if (_index < Game.Scores.Count)
        {
            OnChangeInfo(Game.Scores[_index]);
            OnChangeVisible(true);
        }
        else
        {
            OnChangeVisible(false);
        }
    }

    private void OnChangeInfo(ScoreInfo value)
    {
        _info = value;
        _text.text = _info.score.ToString("F0") + " | " + _info.time;
    }

    private void OnChangeVisible(bool value)
    {
        _visible = value;
        _text.enabled = _visible;
    }
}