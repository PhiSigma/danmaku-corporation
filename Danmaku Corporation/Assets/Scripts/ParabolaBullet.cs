using UnityEngine;
using UnityEngine.Networking;

public struct ParabolaBulletInfo
{
    public float MaxHorizontal;
    public float Distance;
}


public class ParabolaBullet : Bullet
{
    [SyncVar] public ParabolaBulletInfo parabolaInfo;
    
    protected override void SetPosition(float t)
    {
        Vector3 normal = new Vector3(info.Velocity.z, info.Velocity.y, -info.Velocity.x).normalized;

        float x = info.Velocity.magnitude * t;
        float y = parabolaInfo.MaxHorizontal * (1 - Mathf.Pow(2 * x / parabolaInfo.Distance - 1, 2));

        transform.position = info.StartPosition + info.Velocity * t + normal * y;
    }
}