using UnityEngine;

public class EnemyAlert : State
{
    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
    }

    public override void StartState()
    {
        Vector3 pos = transform.position;
        Alarm.Sound(this, new Vector3(pos.x, 0, pos.z));
    }

    public override void UpdateState()
    {
        Vector3 position = _enemy.Target.transform.position;
        _enemy.transform.rotation = Quaternion.LookRotation(position - transform.position);
    }

    public override void EndState()
    {
        
    }
}