using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class ObjectPoolItem
{
    public PoolBehaviour objectToPool;
    public int amountToPool;
}

public class ObjectPooler : NetworkBehaviour
{
    public static ObjectPooler Instance;
    public List<ObjectPoolItem> itemsToPool;
    private Dictionary<string, List<PoolBehaviour>> _pooledObjects;
    private Dictionary<string, ObjectPoolItem> _baseItems;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (!isServer) return;
        
        _baseItems = new Dictionary<string, ObjectPoolItem>();
        _pooledObjects = new Dictionary<string, List<PoolBehaviour>>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            string objectName = item.objectToPool.gameObject.name;
            
            _baseItems.Add(objectName, item);
            _pooledObjects.Add(objectName, new List<PoolBehaviour>());
            
            for (int i = 0; i < item.amountToPool; i++) Create(objectName);
        }
    }
    
    public PoolBehaviour GetPooledObject(string objectName)
    {
        if (!isServer) return null;

        if (!_pooledObjects.ContainsKey(objectName))
        {
            Debug.LogError("Pool does not contain " + objectName + ".");
            return null;
        }
        
        PoolBehaviour obj = _pooledObjects[objectName].FirstOrDefault(t => !t.isActive);
        return obj ? obj : Create(objectName);
    }

    public void ReturnToPool(string objectName, int index)
    {
        if (!isServer) return;
        
        if (!_pooledObjects.ContainsKey(objectName))
        {
            Debug.LogError("Pool does not contain " + objectName + ".");
            return;
        }

        PoolBehaviour obj = _pooledObjects[objectName][index];
        obj.isActive = false;
    }

    private PoolBehaviour Create(string objectName)
    {
        PoolBehaviour objectToPool = _baseItems[objectName].objectToPool;
        
        PoolBehaviour obj = Instantiate(objectToPool);
        obj.Handle = objectName;
        obj.Index = _pooledObjects[objectName].Count;
        _pooledObjects[objectName].Add(obj);
        NetworkServer.Spawn(obj.gameObject);
        
        return obj;
    }
}