using UnityEngine;

public class HitBox : MonoBehaviour
{
    private Character _character;

    private void Start()
    {
        _character = GetComponentInParent<Character>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_character || !_character.isServer) return;

        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet)
        {
            _character.ReduceHealth(bullet.info.Damage);
        }
    }
}