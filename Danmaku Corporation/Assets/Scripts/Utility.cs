using UnityEngine;

public static class Utility
{
    public static Color InterpolateColor(Color c1, Color c2, float t)
    {
        if (t < 0) return c1;
        if (t > 1) return c2;

        Color.RGBToHSV(c1, out float h1, out float s1, out float v1);
        Color.RGBToHSV(c2, out float h2, out float s2, out float v2);

        if (h2 > h1)
        {
            while (Mathf.Abs(h2 - h1) > 0.5f) h2 -= 1;
        }
        else
        {
            while (Mathf.Abs(h2 - h1) > 0.5f) h1 -= 1;
        }

        Vector4 interpolated = Interpolate(new Vector4(h1, s1, v1, c1.a), 
            new Vector4(h2, s2, v2, c2.a), t);

        Color c = Color.HSVToRGB((interpolated[0] + 1) % 1, interpolated[1], interpolated[2]);
        
        return new Color(c.r, c.g, c.b, interpolated[3]);
    }

    private static Vector4 Interpolate(Vector4 a, Vector4 b, float t)
    {
        if (t < 0) return a;
        if (t > 1) return b;

        return a * (1 - t) + b * t;
    }
}