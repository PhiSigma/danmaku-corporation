using UnityEngine;

public class EnemyShooting : Shooting
{
    private bool _isShooting;

    public int[] weaponChangeTimes;
    private float _lastTime;
    private float _helperTime;
    
    private new void Start()
    {
        base.Start();
        
        Enemy enemy = GetComponentInParent<Enemy>();
        if (!enemy)
        {
            Debug.LogError(name + " could not find " + nameof(Enemy));
        }

        if (!enemy) return;
        
        enemy.ParameterChangeEvent += OnChangeParameter;
        _lastTime = Time.time;
    }

    private new void Update()
    {
        base.Update();
        
        if (weaponIndex < 0 || weaponIndex >= weaponChangeTimes.Length) return;

        if (_helperTime > -0.5f || Time.time - _lastTime < weaponChangeTimes[weaponIndex]) return;
        
        weaponIndex = (weaponIndex + 1) % weapons.Count;
        _lastTime = Time.time;
    }

    protected override bool IsShooting()
    {
        return _isShooting;
    }

    private void OnChangeParameter(object sender, EventArgsWithParameter args)
    {
        if (args.Parameter != Parameter.Fire) return;

        _isShooting = args.Value;

        if (_isShooting)
        {
            if (_helperTime < -0.5f) return;
            _lastTime = Time.time - _helperTime;
            _helperTime = -1;
        }
        else
        {
            _helperTime = Time.time - _lastTime;
        }
    }
}