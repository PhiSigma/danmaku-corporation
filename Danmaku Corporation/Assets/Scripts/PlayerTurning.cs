using UnityEngine;

[RequireComponent(typeof(PlayerCharacter))]
public class PlayerTurning : MonoBehaviour
{
    private PlayerCharacter _playerCharacter;
    private Camera _camera;

    public float threshold = 0.1f;
    
    private void Start()
    {
        _playerCharacter = GetComponent<PlayerCharacter>();
        _camera = Camera.main;

        if (!_camera)
        {
            Debug.LogWarning("Camera not found.");
        }
    }

    private void Update()
    {
        if (!_playerCharacter.IsControllable()) return;

        Transform t = _playerCharacter.transform;

        float x, z;

        if (_playerCharacter.controller.HasGamepad || !_camera)
        {
            x = _playerCharacter.controller.GetAxis("Turn Horizontal");
            z = _playerCharacter.controller.GetAxis("Turn Vertical");
        }
        else
        {
            Vector3 playerPosition = _camera.WorldToScreenPoint(t.position);
            Vector3 mouseVector = _playerCharacter.controller.MousePosition - playerPosition;
            x = mouseVector.x;
            z = mouseVector.y;
        }
        
        Vector3 direction = new Vector3(x, 0, z);
        if (direction.magnitude > threshold)
        {
            t.rotation = Quaternion.LookRotation(direction);
        }
    }
}
