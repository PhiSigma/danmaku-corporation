using System;
using UnityEngine;
using UnityEngine.Networking;

public class Visor : NetworkBehaviour
{
    public static event EventHandler<EventArgs> ActivateEvent;
    public static event EventHandler<EventArgs> DeactivateEvent;

    private PlayerCharacter _player;
    
    [SyncVar(hook = nameof(OnChangeActive))] private bool _isActive;

    public const float DarkFactor = 0.8f;

    private void Start()
    {
        _player = GetComponentInParent<PlayerCharacter>();
        if (!_player)
        {
            Debug.LogError(name + " could not find " + nameof(PlayerCharacter));
        }

        Game.GameOverEvent += (sender, args) =>
        {
            ActivateEvent = null;
            DeactivateEvent = null;
        };
    }

    private void Update()
    {
        if (!_player.IsControllable()) return;

        _isActive = _player.controller.GetAxis("Focus") > 0.5f;
    }

    private void OnChangeActive(bool value)
    {
        if (value == _isActive) return;
        
        _isActive = value;
        if (!_player || !_player.controller || !_player.controller.isLocalPlayer) return;

        if (_isActive)
        {
            ActivateEvent?.Invoke(this, new EventArgs());
        }
        else
        {
            DeactivateEvent?.Invoke(this, new EventArgs());
        }
    }
}