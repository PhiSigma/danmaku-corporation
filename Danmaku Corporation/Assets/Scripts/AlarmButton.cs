using UnityEngine;

public class AlarmButton : MonoBehaviour
{
    public float lowering = 0.1f;
    public float lowerSpeed = 1f;

    private Vector3 _originalPosition;
    private Vector3 _targetPosition;

    private void Start()
    {
        _originalPosition = transform.position;
        _targetPosition = _originalPosition;
    }

    private void Update()
    {
        if (Game.IsPaused) return;
        transform.position = Vector3.Lerp(transform.position, _targetPosition, lowerSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.TryGetComponent(out Character character)) return;

        if (!(other.gameObject.TryGetComponent(out Enemy enemy) && !enemy.WantToTriggerAlarm))
        {
            Alarm.Sound(this, transform.position);
        }
        character.DeathEvent += OnDeath;
        _targetPosition = _originalPosition + lowering * Vector3.down;
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.TryGetComponent(out Character character)) return;
        
        character.DeathEvent -= OnDeath;
        _targetPosition = _originalPosition;
    }

    private void OnDeath(object sender, EventArgsWithCharacter e)
    {
        _targetPosition = _originalPosition;
    }
}