using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public float cooldown;
    
    public abstract void Fire(Vector3 position, Vector3 direction);
}