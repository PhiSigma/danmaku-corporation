using UnityEngine;

public class WeaponBasic : Weapon
{
    public float speed = 5f;
    public float radius = 0.2f;
    public Color color = Color.red;
    public float duration = 2f;
    public float damage = 0.1f;
    public Bullet bulletPrefab;
    
    public override void Fire(Vector3 position, Vector3 direction)
    {
        BulletInfo info = new BulletInfo
        {
            StartPosition = position, Rotation = Quaternion.LookRotation(direction), Color = color,
            Radius = radius, Velocity = direction * speed, Duration = duration, Damage = damage
        };

        Bullet bullet = (Bullet) ObjectPooler.Instance.GetPooledObject(bulletPrefab.gameObject.name);
        bullet.info = info;
        bullet.isActive = true;
    }
}