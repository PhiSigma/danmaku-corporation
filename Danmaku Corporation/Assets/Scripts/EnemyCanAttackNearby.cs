using UnityEngine;

public class EnemyCanAttackNearby : State
{
    public bool attack;

    protected Enemy MyEnemy;

    
    protected void Start()
    {
        MyEnemy = GetComponentInParent<Enemy>();
        if (!MyEnemy)
        {
            Debug.LogError(name + " could not find " + nameof(Enemy));
        }
    }

    public override void StartState()
    {
        if (!attack) return;
        
        MyEnemy.ParameterChangeEvent += OnChangeParameter;
        MyEnemy.SetParameter(Parameter.Fire, MyEnemy.Target);
    }

    public override void UpdateState()
    {
        if (!MyEnemy.Target || !attack) return;
        
        Vector3 position = MyEnemy.Target.transform.position;
        MyEnemy.transform.rotation = Quaternion.LookRotation(position - transform.position);
    }

    public override void EndState()
    {
        if (!attack) return;
        
        MyEnemy.ParameterChangeEvent -= OnChangeParameter;
        MyEnemy.SetParameter(Parameter.Fire, false);
    }
    
    
    private void OnChangeParameter(object sender, EventArgsWithParameter e)
    {
        if (e.Parameter != Parameter.Target) return;
        MyEnemy.SetParameter(Parameter.Fire, e.Value);
    }
}