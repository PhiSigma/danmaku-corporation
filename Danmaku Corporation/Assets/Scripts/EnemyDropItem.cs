using UnityEngine;

public class EnemyDropItem : MonoBehaviour
{
    public float[] dropWeights;

    private Enemy _enemy;
    
    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
        if (!_enemy)
        {
            Debug.LogError(name + " could not find " + nameof(Enemy));
        }

        if (!_enemy.isServer) return;

        float r = Random.value;
        float sum = 0;
        int i = 0;
        foreach (Transform child in transform)
        {
            if (i > dropWeights.Length) break;

            sum += dropWeights[i++];
            if (sum < r) continue;
            
            PickupItem item = child.gameObject.GetComponent<PickupItem>();
            if (!item) continue;
            
            item.Initialize();
            _enemy.DeathEvent += (sender, e) => Drop(item);
            break;
        }
    }

    private void Drop(PickupItem item)
    {
        item.gameObject.transform.SetParent(null);
        Vector3 pos = _enemy.transform.position;
        item.Position = new Vector3(pos.x, item.dropHeight, pos.z);
        item.Active = true;
    }
}