using UnityEngine;

public class WeaponFan : WeaponBasic
{
    public int rows = 2;
    public int columns = 3;
    public float horizontalSpread = 45;
    public float originDistance = 0.2f;
    public float verticalDistance = 0.2f;
    public float originSpeed;
    public float secondarySpeed = 0.1f;
    public Color secondaryColor = Color.yellow;
    
    public override void Fire(Vector3 position, Vector3 direction)
    {
        Vector3 baseRotation = Quaternion.LookRotation(direction).eulerAngles;
        
        for (int row = 0; row < rows; row++)
        {
            float d = originDistance + verticalDistance * row;
            float v = originSpeed + secondarySpeed * row;
            Color c = Utility.InterpolateColor(color, secondaryColor, rows > 1 ? row / (rows - 1f) : 0);
            for (int column = 0; column < columns; column++)
            {
                float phi = (column - (columns - 1) / 2f) / ((columns - 1) / 2f) * horizontalSpread;
                Vector3 rotation = baseRotation + Vector3.up * phi;
                Vector3 thisDirection = Quaternion.Euler(rotation) * Vector3.forward;
                Vector3 thisPosition = position + d * thisDirection;
                Vector3 velocity = direction * speed + thisDirection * v;
                
                BulletInfo info = new BulletInfo
                {
                    StartPosition = thisPosition, Rotation = Quaternion.Euler(rotation), Color = c,
                    Radius = radius, Velocity = velocity, Duration = duration, Damage = damage
                };

                Bullet bullet = (Bullet) ObjectPooler.Instance.GetPooledObject(bulletPrefab.gameObject.name);
                bullet.info = info;
                bullet.isActive = true;
            }
        }
    }
}