using UnityEngine;

public class EnemyTargetPlayer : State
{
    public float maxDistance = 6;
    public float walkSpeed = 3.5f;

    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
    }

    public override void StartState()
    {
        _enemy.SetParameter(Parameter.Fire, true);
        
        if (!_enemy.Agent.isActiveAndEnabled) return;
        
        _enemy.Agent.SetDestination(_enemy.Target.transform.position);
        _enemy.Agent.stoppingDistance = maxDistance;
        _enemy.Agent.speed = walkSpeed;
    }

    public override void UpdateState()
    {
        Vector3 position = _enemy.Target.transform.position;
        _enemy.transform.rotation = Quaternion.LookRotation(position - transform.position);
        
        if (_enemy.Agent.isActiveAndEnabled) _enemy.Agent.SetDestination(position);
    }

    public override void EndState()
    {
        _enemy.SetParameter(Parameter.Fire, false);
    }
}