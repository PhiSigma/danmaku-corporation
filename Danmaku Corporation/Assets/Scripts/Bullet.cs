using System;
using UnityEngine;
using UnityEngine.Networking;


public struct BulletInfo
{
    public Vector3 StartPosition;
    public Quaternion Rotation;
    public Vector3 Velocity;
    public float Radius;
    public Color Color;
    public float Duration;
    public float Damage;
}


public class Bullet : PoolBehaviour
{
    private float _startTime;
    
    [SyncVar] private float _helperTime = -1;

    private Collider _collider;
    private Collider Collider
    {
        get
        {
            if (!_collider) _collider = GetComponent<Collider>();
            return _collider;
        }
    }
    
    private static readonly int Emission = Shader.PropertyToID("_EmissionColor");

    [SyncVar(hook = nameof(OnChangeInfo))]
    public BulletInfo info;

    public bool goThroughWall;
    
    
    private void OnChangeInfo(BulletInfo newInfo)
    {
        info = newInfo;
        Transform transform1 = transform;
        transform1.position = info.StartPosition;
        transform1.rotation = info.Rotation;
        transform1.localScale = info.Radius * Vector3.one;
        Renderer.material.SetColor(Emission, info.Color);
    }
    

    protected override void Activate(bool newActive)
    {
        if (newActive)
        {
            _startTime = Time.time;
            transform.position = info.StartPosition;
        }
        
        base.Activate(newActive);
        
        Collider.enabled = isActive;
    }


    private void Start()
    {
        Game.PauseGameEvent += OnPauseGame;
        Game.ContinueGameEvent += OnContinueGame;
    }

    private void OnContinueGame(object sender, EventArgs e)
    {
        if (_helperTime < -0.5f) return;
        _startTime = Time.time - _helperTime;
        _helperTime = -1;
    }

    private void OnPauseGame(object sender, EventArgs e)
    {
        _helperTime = Time.time - _startTime;
        if (_helperTime < 0) _helperTime = 0;
    }

    private void Update()
    {
        if (!isActive || _helperTime > -0.5f) return;
        
        float t = Time.time - _startTime;

        SetPosition(t);

        if (t > info.Duration)
        {
            ObjectPooler.Instance.ReturnToPool(Handle, Index);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger || goThroughWall) return;
        
        ObjectPooler.Instance.ReturnToPool(Handle, Index);
    }


    protected virtual void SetPosition(float t)
    {
        transform.position = info.StartPosition + t * info.Velocity;
    }
}