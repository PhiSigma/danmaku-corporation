using System;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

internal class DarkenRenderer
{
    private readonly Material[] _darkerMaterials;
    private readonly Material[] _originalMaterials;
    private readonly Renderer _renderer;

    internal DarkenRenderer(Renderer renderer)
    {
        _renderer = renderer;
        _originalMaterials = renderer.materials;
        _darkerMaterials = new Material[_originalMaterials.Length];
        for (int i = 0; i < _originalMaterials.Length; i++)
        {
            _darkerMaterials[i] = Object.Instantiate(_originalMaterials[i]);
            Color c = _darkerMaterials[i].color;
            float t = 1 - Visor.DarkFactor;
            Color c2 = new Color(c.r * t, c.g * t, c.b * t, c.a);
            _darkerMaterials[i].color = c2;
        }
    }

    internal void StartDarken()
    {
        _renderer.materials = _darkerMaterials;
    }

    internal void StopDarken()
    {
        _renderer.materials = _originalMaterials;
    }
}

public class VisorDarken : MonoBehaviour
{
    private DarkenRenderer[] _renderers;
    
    private void Start()
    {
        Renderer[] renderers = GetComponentsInChildren<Renderer>().Where(r => !r.GetComponent<ParticleSystem>())
            .ToArray();
        _renderers = new DarkenRenderer[renderers.Length];
        for (int i = 0; i < renderers.Length; i++) _renderers[i] = new DarkenRenderer(renderers[i]);

        Visor.ActivateEvent += OnActivate;
        Visor.DeactivateEvent += OnDeactivate;
    }
    
    private void OnActivate(object sender, EventArgs e)
    {
        foreach (DarkenRenderer r in _renderers) r.StartDarken();
    }
    
    private void OnDeactivate(object sender, EventArgs e)
    {
        foreach (DarkenRenderer r in _renderers) r.StopDarken();
    }

    private void OnDestroy()
    {
        Visor.ActivateEvent -= OnActivate;
        Visor.DeactivateEvent -= OnDeactivate;
    }
}