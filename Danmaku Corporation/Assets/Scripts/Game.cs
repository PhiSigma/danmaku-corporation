using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
internal class ScoreData
{
    public List<ScoreInfo> scores;
}

public class Game : NetworkBehaviour
{
    public static event EventHandler<EventArgs> PauseGameEvent;
    public static event EventHandler<EventArgs> ContinueGameEvent;
    public static event EventHandler<EventArgs> GameOverEvent;

    private static Game _instance;

    private bool _canStart;
    private bool _wantToPause;
    private float _timePassed;
    private float _winTime = -1;
    private float _resetEvents = -1;

    [SyncVar(hook = nameof(OnChangePause))]
    private bool _isPaused = true;

    public static bool IsPaused
    {
        set
        {
            if (_instance.isServer)
            {
                _instance.OnChangePause(value);
            }
            else
            {
                Controller.Instance.CmdSetPause(value);
            }
        }
        get => _instance._isPaused;
    }

    public static bool IsWon { private set; get; }
    public static float Score;
    public static List<ScoreInfo> Scores;

    public float timeScoreFactor = 1000;
    public float timeScoreHalfTime = 60;
    public int maxScores = 5;
    public string scoreSavePath = "scores.txt";


    private void Awake()
    {
        _instance = this;
        IsWon = false;

        Score = 0;
        
        LoadScores();
    }

    private void Start()
    {
        GameOverEvent += (sender, args) => {
            Alarm.Reset();
            _resetEvents = Time.time;
        };
        
        PauseGameEvent?.Invoke(this, new EventArgs());
        
        if (!isServer) return;
        
        _canStart = ControllerManager.PlayerCharacters.All(character => character.controller);

        foreach (PlayerCharacter player in ControllerManager.PlayerCharacters)
        {
            player.DeathEvent += OnPlayerDeath;
        }
    }
    
    private void Update()
    {
        if (_resetEvents > -0.5f && Time.time > _resetEvents + 0.1f)
        {
            GameOverEvent = null;
            ContinueGameEvent = null;
            PauseGameEvent = null;
            _resetEvents = -1;
        }

        if (!isServer) return;

        _canStart = ControllerManager.PlayerCharacters.All(character => character.controller);
        if (!_canStart && !IsPaused) IsPaused = true;
        
        if (!_canStart || IsWon) return;

        if (!IsPaused) _timePassed += Time.deltaTime;
        if (_winTime > -0.5f && _timePassed > _winTime)
        {
            IsWon = true;
            _instance.SaveScore(new ScoreInfo {score = Score, time = DateTime.Now.ToString("g")});
            GameOverEvent?.Invoke(_instance, new EventArgs());
            IsPaused = true;
            return;
        }

        bool newWantToPause = ControllerManager.PlayerCharacters.Any(character =>
        {
            Controller controller = character.controller;
            return controller && controller.GetAxis("Pause") > 0.5f;
        });

        if (newWantToPause && !_wantToPause)
        {
            IsPaused = !IsPaused;
        }

        _wantToPause = newWantToPause;
    }
    

    private void OnChangePause(bool value)
    {
        _isPaused = value;
        
        if (value)
        {
            PauseGameEvent?.Invoke(this, new EventArgs());
        }
        else
        {
            ContinueGameEvent?.Invoke(this, new EventArgs());
        }
    }
 
    private void OnPlayerDeath(object sender, EventArgsWithCharacter e)
    {
        if (!ControllerManager.PlayerCharacters.All(player => player == e.Character || !player)) return;
        
        GameOverEvent?.Invoke(this, new EventArgs());
    }


    private void LoadScores()
    {
        try
        {
            using (StreamReader streamReader = File.OpenText(scoreSavePath))
            {
                string jsonString = streamReader.ReadToEnd();
                Scores = JsonUtility.FromJson
                    <ScoreData>(jsonString).scores;
            }
        }
        catch (IOException)
        {
            Debug.LogWarning("no score save file");
            Scores = new List<ScoreInfo>();
        }
    }
    
    private void SaveScore(ScoreInfo score)
    {
        Scores.Add(score);
        Scores = Scores.OrderByDescending(s => s.score).ToList();
        if (Scores.Count > maxScores) Scores.RemoveAt(maxScores);
        
        string jsonString = JsonUtility.ToJson(new ScoreData {scores = Scores});

        using (StreamWriter streamWriter = File.CreateText(scoreSavePath))
        {
            streamWriter.Write(jsonString);
        }
    }

    
    public static void Reload()
    {
        Score = 0;
        if (!_instance.isServer)
        {
            Controller.Instance.CmdReload();
        }
        else
        {
            CustomNetworkManager.Instance.ServerChangeScene(_instance.gameObject.scene.name);
        }
    }

    public static void EndWithVictory(float waitTime)
    {
        _instance._winTime = _instance._timePassed + waitTime;
        float tFactor = 1 / _instance.timeScoreHalfTime;
        Score += Mathf.Exp(-_instance._timePassed * tFactor) * _instance.timeScoreFactor;
    }
}