using UnityEngine;
using UnityEngine.Networking;

public struct SineBulletInfo
{
    public float Frequency;
    public float Phi;
    public float AmpFactor;
    public float AmpPower;
    public float AmpConst;
}


public class SineBullet : Bullet
{
    [SyncVar] public SineBulletInfo sineInfo;
    
    protected override void SetPosition(float t)
    {
        float horizontal = Amplitude(t) * Mathf.Sin(sineInfo.Frequency * t + sineInfo.Phi * Mathf.PI / 180)
                           - Amplitude(0) * Mathf.Sin(sineInfo.Phi * Mathf.PI / 180);
        Vector3 normal = new Vector3(info.Velocity.z, info.Velocity.y, -info.Velocity.x).normalized;
        transform.position = info.StartPosition + info.Velocity * t + horizontal * normal;
    }

    private float Amplitude(float t)
    {
        return sineInfo.AmpFactor * Mathf.Pow(t, sineInfo.AmpPower) + sineInfo.AmpConst;
    }
}