using System;
using UnityEngine;

public class EventArgsWithPosition : EventArgs
{
    public readonly Vector3 Position;

    public EventArgsWithPosition(Vector3 position)
    {
        Position = position;
    }
}

public static class Alarm
{
    public static event EventHandler<EventArgsWithPosition> AlarmEvent;

    public static void Sound(object sender, Vector3 position)
    {
        AlarmEvent?.Invoke(sender, new EventArgsWithPosition(position));
    }

    public static void Reset()
    {
        AlarmEvent = null;
    }
}