using UnityEngine;
using UnityEngine.UI;

public class CircleSegment : MonoBehaviour
{
    public int resolution = 256;
    public Color color = Color.red;
    public Color color2 = Color.yellow;
    public float size = 3;
    public float spread = 200;
    public float fadeStart = 0.5f;
    public float maxAlpha = 0.5f;
    public float colorFade = 0.1f;
    
    private void Start()
    {
        CircleSegmentBehavior detection = GetComponentInParent<CircleSegmentBehavior>();
        if (detection)
        {
            spread = detection.GetSpread();
            if (detection.GetSubRange() < -0.5f)
            {
                size = detection.GetRange() * 2 / (1 - colorFade);
                color2 = color;
                fadeStart = 1 - 2 * colorFade;
            }
            else
            {
                size = detection.GetRange() * 2;
                fadeStart = detection.GetSubRange() / detection.GetRange();
            }
        }

        PlayerFocusHeal heal = GetComponentInParent<PlayerFocusHeal>();
        if (heal)
        {
            size = heal.maxRange * 2;
            fadeStart = heal.fadeStart;
        }
        
        transform.parent.localScale = Vector3.one * size;
        
        Texture2D tex = new Texture2D(resolution, resolution);
        for (int i = 0; i < resolution; i++)
        {
            for (int j = 0; j < resolution; j++)
            {
                float d = Mathf.Sqrt((i - resolution * 0.5f) * (i - resolution * 0.5f)
                                     + (j - resolution * 0.5f) * (j - resolution * 0.5f)) * 2 / resolution;
                float a = maxAlpha - (d - fadeStart) / (1 - fadeStart) * maxAlpha;
                if (d > 1) a = 0;
                if (d < fadeStart) a = maxAlpha;

                float angle = Mathf.Acos((j - resolution * 0.5f) * 2 / (d * resolution)) * 180 / Mathf.PI;
                if (Mathf.Abs(spread - angle) < 5f)
                {
                    a *= (spread - angle + 5f) / 10f;
                } 
                else if (angle > spread)
                {
                    a = 0;
                }

                float t = (d - fadeStart + colorFade) / (2*colorFade);
                Color c = Utility.InterpolateColor(color, color2, t);
                
                tex.SetPixel(i, j, new Color(c.r, c.g, c.b, a));
            }
        }

        tex.Apply();
        
        RawImage image = GetComponent<RawImage>();
        image.texture = tex;
    }
}