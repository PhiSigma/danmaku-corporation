using System;
using UnityEngine;
using UnityEngine.Networking;


public class EventArgsWithCharacter : EventArgs
{
    public readonly Character Character;

    public EventArgsWithCharacter(Character character)
    {
        Character = character;
    }
}


[RequireComponent(typeof(Rigidbody))]
public class Character : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnHealthChange))] private float _health = 1;
    
    private HealthBar HealthBar { set; get; }
    
    public Rigidbody RigidBody { private set; get; }
    public Animator Animator { private set; get; }

    public event EventHandler<EventArgsWithCharacter> DeathEvent;

    public float maxHealth = 1;


    protected void Start()
    {
        RigidBody = GetComponent<Rigidbody>();
        Animator = GetComponent<Animator>();
        HealthBar = GetComponentInChildren<HealthBar>();
        if (!HealthBar)
        {
            Debug.LogError(name + " could not find " + nameof(HealthBar));
        }
    }

    protected void Update()
    {
        if (!isServer || !Animator) return;

        foreach (AnimatorControllerParameter parameter in Animator.parameters)
        {
            string parameterName = parameter.name;
            
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Float:
                    RpcSyncAnimatorFloat(parameterName, Animator.GetFloat(parameterName));
                    break;
                case AnimatorControllerParameterType.Int:
                    RpcSyncAnimatorInt(parameterName, Animator.GetInteger(parameterName));
                    break;
                case AnimatorControllerParameterType.Bool:
                    RpcSyncAnimatorBool(parameterName, Animator.GetBool(parameterName));
                    break;
                case AnimatorControllerParameterType.Trigger:
                    RpcSyncAnimatorTrigger(parameterName);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }


    [ClientRpc]
    private void RpcSyncAnimatorFloat(string parameterName, float value)
    {
        if (isServer || !Animator) return;
        Animator.SetFloat(parameterName, value);
    }

    [ClientRpc]
    private void RpcSyncAnimatorInt(string parameterName, int value)
    {
        if (isServer || !Animator) return;
        Animator.SetInteger(parameterName, value);
    }

    [ClientRpc]
    private void RpcSyncAnimatorBool(string parameterName, bool value)
    {
        if (isServer || !Animator) return;
        Animator.SetBool(parameterName, value);
    }
    
    [ClientRpc]
    private void RpcSyncAnimatorTrigger(string parameterName)
    {
        if (isServer || !Animator) return;
        Animator.SetTrigger(parameterName);
    }


    private void OnHealthChange(float value)
    {
        HealthBar.Value = value;
    }

    
    protected virtual void Die()
    {
        Destroy(gameObject);
    }


    public virtual bool IsControllable()
    {
        return !Game.IsPaused && isServer;
    }

    public void ReduceHealth(float value)
    {
        _health -= value / maxHealth;
        if (_health > 1) _health = 1;
        HealthBar.Value = _health;

        if (_health > 0) return;
        
        DeathEvent?.Invoke(this, new EventArgsWithCharacter(this));
        Die();
    }
}