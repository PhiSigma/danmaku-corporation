using UnityEngine;
using UnityEngine.Networking;

public abstract class PoolBehaviour : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnChangeActive))] public bool isActive;
    [SyncVar] public int Index;
    
    private Renderer _renderer;
    protected Renderer Renderer
    {
        get
        {
            if (!_renderer) _renderer = GetComponent<Renderer>();
            return _renderer;
        }
    }
    
    //public int Index { set; protected get; }
    public string Handle { set; protected get; }

    
    private void Awake()
    {
        OnChangeActive(false);
    }


    private void OnChangeActive(bool newActive)
    {
        Activate(newActive);
    }
    
    
    protected virtual void Activate(bool newActive)
    {
        isActive = newActive;
        Renderer.enabled = isActive;
    }
}