using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    public float damagePerSecond;
    private List<PlayerCharacter> _hurting;

    private void Start()
    {
        _hurting = new List<PlayerCharacter>();
    }

    private void Update()
    {
        foreach (PlayerCharacter player in _hurting)
        {
            if (!player) continue;
            if (!player.isServer) return;
            player.ReduceHealth(damagePerSecond * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerCharacter player = other.gameObject.GetComponent<PlayerCharacter>();
        if (player && player.isServer)
        {
            _hurting.Add(player);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerCharacter player = other.gameObject.GetComponent<PlayerCharacter>();
        if (player && player.isServer)
        {
            _hurting.Remove(player);
        }
    }
}