﻿using System.Linq;
using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    public static PlayerCharacter[] PlayerCharacters { private set; get; }
    
    public PlayerCharacter[] playerCharacters;
    public string[] axisNames;


    private void Start()
    {
        PlayerCharacters = playerCharacters;
    }
    
    
    public static void AddController(Controller controller)
    {
        PlayerCharacter firstUncontrolled = PlayerCharacters.FirstOrDefault(c => c && !c.controller);
        
        if (!firstUncontrolled) return;
        
        firstUncontrolled.controller = controller;
    }

    public static void RemoveController(Controller controller)
    {
        PlayerCharacter controlledByThis = PlayerCharacters.FirstOrDefault(c => c && c.controller.Equals(controller));
        
        if (!controlledByThis) return;

        controlledByThis.controller = null;
    }

    public static string[] GetAxisNames()
    {
        ControllerManager instance = FindObjectOfType<ControllerManager>();
        
        if (instance) return instance.axisNames;
        
        Debug.LogError("Controller manager not found.");
        return new string[0];
    }
}
