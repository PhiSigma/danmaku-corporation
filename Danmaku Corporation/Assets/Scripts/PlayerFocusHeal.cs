using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerCharacter))]
public class PlayerFocusHeal : CircleSegmentBehavior
{
    public float healPerSecond = 0.05f;
    public float maxRange = 5;
    public float fadeStart = 0.5f;
    public ParticleSystem[] particleSystems;

    private PlayerCharacter _player;
    private List<PlayerCharacter> _healable;

    [SyncVar(hook = nameof(OnChangeFocus))]
    private bool _isFocused;
    
    private void Start()
    {
        _player = GetComponent<PlayerCharacter>();
        _healable = new List<PlayerCharacter>();
        foreach (PlayerCharacter player in ControllerManager.PlayerCharacters)
        {
            if (player != _player) _healable.Add(player);
        }
    }

    private void Update()
    {
        if (!_player.IsControllable()) return;

        bool isFocused = _player.controller.GetAxis("Focus") > 0.5f;
        
        if (isFocused != _isFocused)
        {
            foreach (ParticleSystem system in particleSystems)
            {
                if (isFocused)
                {
                    system.Play();
                }
                else
                {
                    system.Stop();
                }
            }
            _isFocused = isFocused;
        }

        if (!_isFocused) return;
        
        foreach (ParticleSystem system in particleSystems)
        {
            system.transform.rotation = Quaternion.identity;
        }
        
        foreach (PlayerCharacter player in _healable)
        {
            float d = (player.transform.position - transform.position).magnitude / maxRange;
            if (d > 1) return;
            
            float a = 1 - (d - fadeStart) / (1 - fadeStart);
            if (d < fadeStart) a = 1;

            player.ReduceHealth(-a * healPerSecond * Time.deltaTime);
        }
    }
    
    
    public override float GetRange()
    {
        return maxRange;
    }

    public override float GetSubRange()
    {
        return maxRange * fadeStart;
    }

    public override float GetSpread()
    {
        return 200;
    }


    private void OnChangeFocus(bool value)
    {
        _isFocused = value;
        
        foreach (ParticleSystem system in particleSystems)
        {
            if (_isFocused)
            {
                system.Play();
            }
            else
            {
                system.Stop();
            }
        }
    }
}