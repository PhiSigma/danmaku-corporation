using System;
using UnityEngine;

public class VisorVisible : MonoBehaviour
{
    private CanvasGroup _cg;
    
    private void Start()
    {
        _cg = GetComponent<CanvasGroup>();
        if (!_cg) return;
        Visor.ActivateEvent += OnActivate;
        Visor.DeactivateEvent += OnDeactivate;
    }

    private void OnActivate(object sender, EventArgs e)
    {
        _cg.alpha = 1;
    }
    
    private void OnDeactivate(object sender, EventArgs e)
    {
        _cg.alpha = 0;
    }

    private void OnDestroy()
    {
        Visor.ActivateEvent -= OnActivate;
        Visor.DeactivateEvent -= OnDeactivate;
    }
}