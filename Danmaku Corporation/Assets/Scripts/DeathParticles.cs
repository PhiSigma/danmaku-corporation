using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class DeathParticles : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnChangeActive))]
    private bool _active;

    private float _startTime;

    private ParticleSystem[] _particleSystems;

    private void Start()
    {
        _particleSystems = GetComponentsInChildren<ParticleSystem>();

        if (!isServer) return;
        
        Character character = GetComponentInParent<Character>();
        if (!character)
        {
            Debug.LogWarning(name + " could not find " + nameof(Character));
        }
        character.DeathEvent += (sender, withCharacter) => OnChangeActive(true);
    }

    private void Update()
    {
        if (!_active) return;

        foreach (ParticleSystem system in _particleSystems)
        {
            if (system && Time.time - _startTime > system.main.duration)
            {
                Destroy(system.gameObject);
            }
        }
        
        if (_particleSystems.All(system => !system)) Destroy(gameObject);
    }

    private void OnChangeActive(bool value)
    {
        if (_active == value) return;

        if (value && !_active)
        {
            transform.SetParent(null);
            foreach (ParticleSystem system in _particleSystems)
            {
                system.Play();
            }

            _startTime = Time.time;
        }
        
        _active = value;
    }
}