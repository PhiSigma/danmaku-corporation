﻿using UnityEngine;

[RequireComponent(typeof(PlayerCharacter))]
public class PlayerWalking : MonoBehaviour
{
    private static readonly int Forward = Animator.StringToHash("Forward");
    
    private PlayerCharacter _playerCharacter;

    public float walkSpeed = 1f;
    public float focusFactor = 0.5f;

    private void Start()
    {
        _playerCharacter = GetComponent<PlayerCharacter>();
    }

    private void Update()
    {
        if (!_playerCharacter.IsControllable()) return;

        float x = _playerCharacter.controller.GetAxis("Horizontal");
        float z = _playerCharacter.controller.GetAxis("Vertical");
        Vector3 movement = new Vector3(x, 0, z);

        bool focused = _playerCharacter.controller.GetAxis("Focus") > 0.5f;

        _playerCharacter.RigidBody.velocity = movement * walkSpeed * (focused ? focusFactor : 1);

        if (_playerCharacter.Animator)
        {
            _playerCharacter.Animator.SetFloat(Forward, movement.magnitude);
        }
    }
}
