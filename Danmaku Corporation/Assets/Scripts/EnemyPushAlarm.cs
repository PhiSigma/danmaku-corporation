using UnityEngine;

public class EnemyPushAlarm : EnemyCanAttackNearby
{
    public float walkSpeed = 6.5f;

    private AlarmButton[] _buttons;
    
    
    private new void Start()
    {
        base.Start();
        
        _buttons = FindObjectsOfType<AlarmButton>();
        if (_buttons.Length == 0)
        {
            Debug.LogError(name + " needs at least one " + nameof(AlarmButton));
        }
    }

    public override void StartState()
    {
        MyEnemy.WantToTriggerAlarm = true;
        
        AlarmButton closest = _buttons[0];
        float minDist = (closest.transform.position - transform.position).magnitude;
        foreach (AlarmButton button in _buttons)
        {
            float dist = (button.transform.position - transform.position).magnitude;
            if (dist >= minDist) continue;
            minDist = dist;
            closest = button;
        }

        MyEnemy.Agent.SetDestination(closest.transform.position);
        MyEnemy.Agent.stoppingDistance = 0;
        MyEnemy.Agent.speed = walkSpeed;
    }

    public override void EndState()
    {
        base.EndState();

        MyEnemy.WantToTriggerAlarm = false;
    }
}