using UnityEngine;

public class MultiWeapon : Weapon
{
    public Weapon[] weapons;
    public float[] angleOffsets;
    private float[] _lastTimes;

    private void Start()
    {
        _lastTimes = new float[weapons.Length];

        for (int i = 0; i < weapons.Length; i++)
        {
            _lastTimes[i] = -weapons[i].cooldown;
        }
    }

    public override void Fire(Vector3 position, Vector3 direction)
    {
        float time = Time.time;

        for (int i = 0; i < weapons.Length; i++)
        {
            if (!(time - _lastTimes[i] > weapons[i].cooldown)) continue;
            
            Vector3 dir = i >= angleOffsets.Length ? direction : 
                    Quaternion.Euler(0, Quaternion.LookRotation(direction).eulerAngles.y + angleOffsets[i], 0) 
                    * Vector3.forward;
            
            weapons[i].Fire(position, dir);
            _lastTimes[i] = time;
        }
    }
}