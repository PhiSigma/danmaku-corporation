using UnityEngine;

public class PlayerCharacter : Character
{
    public Controller controller;
    private Vector3 _lastPosition;

    private new void Start()
    {
        base.Start();
        _lastPosition = transform.position;
    }

    private void LateUpdate()
    {
        if (isServer && !(controller && controller.IsReady && !Game.IsPaused))
        {
            transform.position = _lastPosition;
        }
        
        _lastPosition = transform.position;
    }

    public override bool IsControllable()
    {
        return base.IsControllable() && controller && controller.IsReady;
    }
}