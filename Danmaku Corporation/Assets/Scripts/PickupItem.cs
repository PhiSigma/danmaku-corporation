using UnityEngine;
using UnityEngine.Networking;

public abstract class PickupItem : NetworkBehaviour
{
    public float dropHeight;

    [SyncVar(hook = nameof(OnChangeActive))]
    private bool _active;

    [SyncVar(hook = nameof(OnChangePosition))]
    private Vector3 _position;
    
    public bool Active
    {
        set => OnChangeActive(value);
    }
    
    public Vector3 Position
    {
        set => OnChangePosition(value);
    }

    private Collider _collider;
    
    private Collider Collider
    {
        get
        {
            if (!_collider) _collider = GetComponent<Collider>();
            return _collider;
        }
    }

    protected abstract void Pickup(PlayerCharacter player);

    public virtual void Initialize()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        PlayerCharacter player = other.gameObject.GetComponent<PlayerCharacter>();
        
        if (!player || !isServer) return;
        
        Pickup(player);
        Destroy(gameObject);
    }

    private void OnChangeActive(bool value)
    {
        if (_active == value) return;
        
        if (value && !_active) transform.SetParent(null);
        _active = value;
        transform.GetChild(0).gameObject.SetActive(value);
        Collider.enabled = value;
    }

    private void OnChangePosition(Vector3 value)
    {
        if (transform.position == value) return;
        
        _position = value;
        transform.position = _position;
    }
}