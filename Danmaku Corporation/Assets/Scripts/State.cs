using System;
using UnityEngine;


[Serializable]
public enum Parameter
{
    Target, Alarm, Fire
}

[Serializable]
public struct Condition
{
    public Parameter parameter;
    public bool value;
}

[Serializable]
public struct Transition
{
    public int from;
    public int to;
    public Condition[] conditions;
}


public abstract class State : MonoBehaviour
{
    public abstract void StartState();
    public abstract void UpdateState();
    public abstract void EndState();
}