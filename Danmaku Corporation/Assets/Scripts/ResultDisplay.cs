using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class ResultDisplay : NetworkBehaviour
{
    private CanvasGroup _canvasGroup;

    [SyncVar(hook=nameof(OnChangeVisible))] public bool visible;
    [SyncVar(hook=nameof(OnChangeScore))] public float score;

    public Text winText;
    public Text loseText;
    
    private bool _addedScore;
    
    private void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();

        if (!isServer)
        {
            _canvasGroup.alpha = visible ? 1 : 0;
            return;
        }
        
        Game.GameOverEvent += (sender, args) => OnChangeVisible(true);
    }

    private void OnChangeVisible(bool value)
    {
        visible = value;
        _canvasGroup.alpha = value ? 1 : 0;

        if (isServer)
        {
            OnChangeScore(Game.IsWon ? Game.Score : -1);
        }
    }
    
    private void OnChangeScore(float value)
    {
        score = value;
        
        if (score < -0.5f)
        {
            loseText.gameObject.SetActive(true);
        }
        else
        {
            if (!_addedScore)
            {
                winText.text += value.ToString("F0");
                _addedScore = true;
            }
            winText.gameObject.SetActive(true);
        }
    }
}