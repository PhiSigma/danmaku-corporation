using UnityEngine;

public class WeaponParabola : WeaponBasic
{
    private Enemy _enemy;

    public float maxHorizontal = 4;
    public int n = 4;
    public Color secondaryColor = Color.yellow;

    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
        if (!_enemy)
        {
            Debug.LogWarning(name + " cannot find " + nameof(Enemy));
        }
    }

    public override void Fire(Vector3 position, Vector3 direction)
    {
        if (!_enemy) _enemy = GetComponentInParent<Enemy>();
        
        if (!(bulletPrefab is ParabolaBullet))
        {
            Debug.LogError(name + " cannot handle " + bulletPrefab.name 
                           + ", should be " + nameof(ParabolaBullet));
            return;
        }

        float distance = (_enemy.Target.transform.position - position).magnitude;
        
        for (int i = 0; i < n; i++)
        {
            float maxH = maxHorizontal * (i - (n - 1) / 2f) * 2f / (n - 1);

            Color c = Utility.InterpolateColor(color, secondaryColor, 1 - maxH / maxHorizontal);
             
            BulletInfo info = new BulletInfo
            {
                StartPosition = position, Rotation = Quaternion.LookRotation(direction), Color = c,
                Radius = radius, Velocity = direction * speed, Duration = duration, Damage = damage
            };
            
            ParabolaBulletInfo parabolaInfo = new ParabolaBulletInfo
            {
                MaxHorizontal = maxH, Distance = distance
            };

            ParabolaBullet bullet = (ParabolaBullet) ObjectPooler.Instance.GetPooledObject(bulletPrefab.gameObject.name);
            bullet.info = info;
            bullet.parabolaInfo = parabolaInfo;
            bullet.isActive = true;
        }
    }
}