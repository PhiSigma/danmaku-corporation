using UnityEngine;

public class PlayerShooting : Shooting
{
    private PlayerCharacter _playerCharacter;
    private int _wantToSwitch;
    
    private new void Start()
    {
        base.Start();
        
        _playerCharacter = GetComponentInParent<PlayerCharacter>();
        if (!_playerCharacter)
        {
            Debug.LogError(name + " could not find " + nameof(PlayerCharacter));
        }
    }

    private new void Update()
    {
        base.Update();
        
        if (!_playerCharacter.IsControllable()) return;

        float weaponSwitch = _playerCharacter.controller.GetAxis("Weapon");
        
        int newWantToSwitch = 0;
        if (weaponSwitch > 0.3f) newWantToSwitch = 1;
        if (weaponSwitch < -0.3f) newWantToSwitch = -1;
        
        if (newWantToSwitch == _wantToSwitch) return;

        _wantToSwitch = newWantToSwitch;

        if (_wantToSwitch == 0) return;
        
        weaponIndex += _wantToSwitch;
        int n = weapons.Count;
        if (weaponIndex < 0) weaponIndex += n;
        if (weaponIndex >= n) weaponIndex -= n;
    }

    protected override bool IsShooting()
    {
        return _playerCharacter.controller.GetAxis("Fire") > 0.5f;
    }
}