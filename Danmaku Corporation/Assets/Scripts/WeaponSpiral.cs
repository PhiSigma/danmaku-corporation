using UnityEngine;

public class WeaponSpiral : WeaponBasic
{
    public float frequency = 1;
    public float ampFactor = 1;
    public float ampPower = 2;
    
    public override void Fire(Vector3 position, Vector3 direction)
    {
        if (!(bulletPrefab is SpiralBullet))
        {
            Debug.LogError(name + " cannot handle " + bulletPrefab.name 
                           + ", should be " + nameof(SpiralBullet));
            return;
        }
        
        BulletInfo info = new BulletInfo
        {
            StartPosition = position, Rotation = Quaternion.LookRotation(direction), Color = color,
            Radius = radius, Velocity = direction * speed, Duration = duration, Damage = damage
        };

        SpiralBulletInfo spiralInfo = new SpiralBulletInfo
        {
            Frequency = frequency, AmpPower = ampPower, AmpFactor = ampFactor
        };

        SpiralBullet bullet = (SpiralBullet) ObjectPooler.Instance.GetPooledObject(bulletPrefab.gameObject.name);
        bullet.info = info;
        bullet.spiralInfo = spiralInfo;
        bullet.isActive = true;
    }
}