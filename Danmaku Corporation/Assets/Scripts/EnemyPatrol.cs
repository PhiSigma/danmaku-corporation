﻿using UnityEngine;

public class EnemyPatrol : State
{
    public Transform path;
    public float targetDistance = 4f;
    public float stoppingDistance = 1f;
    public float walkSpeed = 2.5f;

    private Enemy _enemy;
    private Vector3[] _wayPoints;
    private int _index;
    
    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
        if (!_enemy)
        {
            Debug.LogError(name + " could not find " + nameof(Enemy));
        }
        
        _wayPoints = new Vector3[path.childCount];
        int i = 0;
        foreach (Transform child in path)
        {
            _wayPoints[i++] = child.position;
        }
    }

    
    public override void StartState()
    {
        if (!_enemy.Agent.isActiveAndEnabled) return;
        
        _enemy.Agent.stoppingDistance = stoppingDistance;
        _enemy.Agent.speed = walkSpeed;

        if (_wayPoints.Length == 0) return;

        float minDistance = (_wayPoints[_index] - transform.position).magnitude;
        for (int i = 0; i < _wayPoints.Length; i++)
        {
            float dist = (_wayPoints[i] - transform.position).magnitude;
            if (dist >= minDistance) continue;
            minDistance = dist;
            _index = i;
        }
        
        _enemy.Agent.SetDestination(_wayPoints[_index]);
    }

    public override void UpdateState()
    {
        if (!_enemy.Agent.isActiveAndEnabled) return;
        
        int n = _wayPoints.Length;
        
        if (n == 0 || _enemy.Agent.remainingDistance > targetDistance) return;

        _index = (_index + 1) % n;
        _enemy.Agent.SetDestination(_wayPoints[_index]);
    }

    public override void EndState()
    {
        
    }
}
