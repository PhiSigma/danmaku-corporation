using System.Collections.Generic;
using UnityEngine;

public abstract class Shooting : MonoBehaviour
{
    private Character _character;
    private float _lastShot;
    
    private static readonly int Fire = Animator.StringToHash("Fire");
    
    public float offset = 0.5f;

    public List<Weapon> weapons;
    public int weaponIndex;

    protected void Start()
    {
        _character = GetComponentInParent<Character>();
        if (!_character)
        {
            Debug.LogError(name + " could not find " + nameof(Character));
        }


        if (weaponIndex >= 0 && weaponIndex < weapons.Count)
        {
            _lastShot = -weapons[weaponIndex].cooldown;
        }
    }

    protected void Update()
    {
        if (!_character || !_character.IsControllable()) return;

        if (weaponIndex < 0 || weaponIndex >= weapons.Count) return;
        Weapon weapon = weapons[weaponIndex];

        bool isShooting = IsShooting();
        
        if (_character.Animator)
        {
            _character.Animator.SetBool(Fire, isShooting);
        }
        
        if (!isShooting || Time.time - _lastShot < weapon.cooldown) return;

        Transform t = _character.transform;
        Vector3 forward = t.forward;
        Vector3 position = transform.position + forward * offset;
        
        weapon.Fire(position, forward);

        _lastShot = Time.time;
    }

    protected abstract bool IsShooting();
}