public class HealItem : PickupItem
{
    public float healValue = 0.2f;
    
    protected override void Pickup(PlayerCharacter player)
    {
        player.ReduceHealth(-healValue);
    }
}