using UnityEngine;

public class EnemyTurn : State
{
    private float _time;
    private float _phi;
    private float _startAngle;
    private float _endAngle;
    private Enemy _enemy;

    public float turnSpeed = 1;
    public float angle = 90;

    private void Start()
    {
        _enemy = GetComponentInParent<Enemy>();
        _startAngle = _enemy.transform.rotation.eulerAngles.y;
        _endAngle = _startAngle + angle;
        _time = 0;
    }

    public override void StartState()
    {
        float y = _enemy.transform.eulerAngles.y;
        float x = (y - (_startAngle + _endAngle) / 2) * 2 / (_startAngle - _endAngle);
        if (x < -1) x = -1;
        if (x > 1) x = 1;
        _phi = _time * turnSpeed - Mathf.Acos(x);
    }

    public override void UpdateState()
    {
        _time += Time.deltaTime;
        
        float y = (_startAngle + _endAngle) / 2 
                  + (_startAngle - _endAngle) / 2 * Mathf.Cos(_time * turnSpeed - _phi);

        Vector3 euler = _enemy.transform.rotation.eulerAngles;
        _enemy.transform.rotation = Quaternion.Euler(euler.x, y, euler.z);
    }

    public override void EndState()
    {
        
    }
}