using System;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyIsBoss : MonoBehaviour
{
    public float waitTime = 5;
    private Enemy _enemy;
    
    private void Start()
    {
        _enemy = GetComponent<Enemy>();
        _enemy.DeathEvent += OnDeath;
    }

    private void OnDeath(object sender, EventArgs args)
    {
        if (_enemy.isServer) Game.EndWithVictory(waitTime);   
    }
}