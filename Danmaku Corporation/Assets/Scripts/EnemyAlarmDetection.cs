using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyAlarmDetection : MonoBehaviour
{
    public float maxDistance = 100;
    public float detectionEndRadius = 5;
    public float cooldown;

    private Enemy _enemy;
    private bool _isAlarmed;
    private float _alarmTime;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
        _enemy.ParameterChangeEvent += OnParameterChange;
        _enemy.DeathEvent += OnDeath;
        Alarm.AlarmEvent += OnAlarm;
    }

    private void OnDeath(object sender, EventArgsWithCharacter e)
    {
        Alarm.AlarmEvent -= OnAlarm;
    }

    private void OnParameterChange(object sender, EventArgsWithParameter e)
    {
        if (e.Parameter != Parameter.Alarm) return;
        _isAlarmed = e.Value;
    }

    private void OnAlarm(object sender, EventArgsWithPosition e)
    {
        if ((e.Position - transform.position).magnitude > maxDistance) return;
        
        _enemy.TargetPosition = e.Position;
        _enemy.SetParameter(Parameter.Alarm, true);
        _alarmTime = Time.time;
    }

    private void Update()
    {
        if (!_enemy.IsControllable()) return;

        if (!_isAlarmed || Time.time - _alarmTime < cooldown) return;

        if ((_enemy.TargetPosition - transform.position).magnitude > detectionEndRadius) return;
        
        _enemy.SetParameter(Parameter.Alarm, false);
    }
}