using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CanvasGroup))]
public class Menu : NetworkBehaviour
{
    private CanvasGroup _canvasGroup;

    [SyncVar(hook=nameof(OnChangeVisible))] public bool visible;
    
    private void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();

        if (!isServer)
        {
            _canvasGroup.alpha = visible ? 1 : 0;
            _canvasGroup.interactable = visible;
            _canvasGroup.blocksRaycasts = visible;
            return;
        }
        
        OnChangeVisible(Game.IsPaused);

        Game.PauseGameEvent += (sender, args) => OnChangeVisible(true);
        Game.ContinueGameEvent += (sender, args) => OnChangeVisible(false);
        Game.GameOverEvent += (sender, args) => OnChangeVisible(true);
    }

    private void OnChangeVisible(bool value)
    {
        visible = value;
        _canvasGroup.alpha = visible ? 1 : 0;
        _canvasGroup.interactable = visible;
        _canvasGroup.blocksRaycasts = visible;
    }
}