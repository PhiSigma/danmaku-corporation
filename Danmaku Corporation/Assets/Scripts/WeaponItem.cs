using System;
using System.Reflection;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponItem : PickupItem
{
    public Weapon weapon1;
    public Weapon weapon2;
    private Weapon _pickupWeapon;

    public override void Initialize()
    {
        Weapon baseWeapon, secondaryWeapon;
        if (Random.value < 0.5f)
        {
            baseWeapon = weapon1;
            secondaryWeapon = weapon2;
        }
        else
        {
            baseWeapon = weapon2;
            secondaryWeapon = weapon1;
        }

        _pickupWeapon = Instantiate(baseWeapon);
        
        Type type = baseWeapon.GetType();
        Type otherType = secondaryWeapon.GetType();
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo info in fields)
        {
            FieldInfo other = otherType.GetField(info.Name);
            if (other == null || info.FieldType != other.FieldType) continue;

            if (info.FieldType == typeof(int))
            {
                int val1 = (int) info.GetValue(baseWeapon);
                int val2 = (int) other.GetValue(secondaryWeapon);
                info.SetValue(_pickupWeapon, Random.Range(val1, val2+1));
            } 
            else if (info.FieldType == typeof(Color))
            {
                Color val1 = (Color) info.GetValue(baseWeapon);
                Color val2 = (Color) other.GetValue(secondaryWeapon);
                info.SetValue(_pickupWeapon, Utility.InterpolateColor(val1, val2, Random.value));
            } 
            else if (info.FieldType == typeof(float))
            {
                float val1 = (float) info.GetValue(baseWeapon);
                float val2 = (float) other.GetValue(secondaryWeapon);
                info.SetValue(_pickupWeapon, Random.Range(val1, val2));
            }
            else if (!(info.FieldType == typeof(Bullet)|| info.FieldType.IsSubclassOf(typeof(Bullet))))
            {
                Debug.LogWarning("field type " + info.FieldType.Name + " is not handled");
            }
        }
    }

    protected override void Pickup(PlayerCharacter player)
    {
        PlayerShooting playerShooting = player.GetComponentInChildren<PlayerShooting>();
        if (!playerShooting)
        {
            Debug.LogError(name + " could not find " + nameof(PlayerShooting));
            return;
        }
        if (!_pickupWeapon) Initialize();
        playerShooting.weapons.Add(_pickupWeapon);
        playerShooting.weaponIndex = playerShooting.weapons.Count - 1;
    }
}