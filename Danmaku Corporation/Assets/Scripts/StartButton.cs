using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StartButton : NetworkBehaviour
{
    private Button _button;
    private Text _text;

    public string startText = "START";
    public string continueText = "CONTINUE";
    public string reloadText = "RESTART";

    [SyncVar] private bool _gameOver;
    
    [SyncVar(hook = nameof(OnCanPlay))] public bool canPlay;
    [SyncVar(hook = nameof(OnChangeText))] public string text;


    private void Awake()
    {
        _button = GetComponent<Button>();
    }
    
    private void Start()
    {
        _button.onClick.AddListener(OnClick);
        _button.interactable = canPlay;

        _text = GetComponentInChildren<Text>();
        
        if (!isServer)
        {
            _text.text = text;
            return;
        }

        OnChangeText(startText);
        canPlay = ControllerManager.PlayerCharacters.All(character => character.controller);
        RpcActivateButton(canPlay);

        Game.ContinueGameEvent += (sender, args) => OnChangeText(continueText);
        Game.GameOverEvent += (sender, args) =>
        {
            OnChangeText(reloadText);
            canPlay = true;
            _gameOver = true;
        };

        //ControllerManager.NotAllReadyEvent += (sender, args) => OnCanPlay(false);
        //ControllerManager.AllPlayersReadyEvent += (sender, args) => OnCanPlay(true);
    }

    private void Update()
    {
        if (isServer)
        {
            OnCanPlay(ControllerManager.PlayerCharacters.All(character => character.controller));
        }
    }


    private void OnClick()
    {
        if (_gameOver)
        {
            Game.Reload();
        }
        else
        {
            Game.IsPaused = false;
        }
    }

    private void OnCanPlay(bool value)
    {
        canPlay = value;
        _button.interactable = value;
    }

    private void OnChangeText(string value)
    {
        text = value;
        _text.text = value;
    }

    [ClientRpc]
    private void RpcActivateButton(bool value)
    {
        OnCanPlay(value);
    }
}