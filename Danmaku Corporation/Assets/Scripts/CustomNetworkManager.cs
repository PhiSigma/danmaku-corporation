﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager
{
    public static CustomNetworkManager Instance;
    private static List<Controller> _controllers;
    
    private void Awake()
    {
        Instance = this;
        _controllers = new List<Controller>();
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject player = Instantiate(playerPrefab);

        Controller controller = player.GetComponent<Controller>();
        ControllerManager.AddController(controller);
        _controllers.Add(controller);
        
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }

    public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
    {
        Controller controller = player.gameObject.GetComponent<Controller>();
        ControllerManager.RemoveController(controller);
        _controllers.Remove(controller);
        
        base.OnServerRemovePlayer(conn, player);
    }

    public override void ServerChangeScene(string newSceneName)
    {
        base.ServerChangeScene(newSceneName);

        foreach (Controller controller in _controllers)
        {
            ControllerManager.AddController(controller);
        }
    }
}