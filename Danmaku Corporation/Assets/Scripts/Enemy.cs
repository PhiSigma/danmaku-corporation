﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;


public class EventArgsWithParameter : EventArgs
{
    public readonly Parameter Parameter;
    public readonly bool Value;

    public EventArgsWithParameter(Parameter parameter, bool value)
    {
        Parameter = parameter;
        Value = value;
    }
}

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : Character
{
    private int _stateIndex = -1;

    public float killScore = 100;
    public State[] states;
    public Transition[] transitions;
    public bool WantToTriggerAlarm { set; get; }

    public event EventHandler<EventArgsWithCharacter> TargetChangeEvent;
    public event EventHandler<EventArgsWithPosition> TargetPositionChangeEvent;
    public event EventHandler<EventArgsWithParameter> ParameterChangeEvent;
    
    public NavMeshAgent Agent { private set; get; }

    private PlayerCharacter _target;
    public PlayerCharacter Target
    {
        get => _target;
        set
        {
            _target = value;
            SetParameter(Parameter.Target, _target);
            TargetChangeEvent?.Invoke(this, new EventArgsWithCharacter(_target));
        }
    }
    
    private Vector3 _targetPosition;
    public Vector3 TargetPosition
    {
        get => _targetPosition;
        set
        {
            _targetPosition = value;
            TargetPositionChangeEvent?.Invoke(this, new EventArgsWithPosition(_targetPosition));
        }
    }

    private Dictionary<Parameter, bool> _parameters;

    
    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
    }

    private new void Start()
    {
        base.Start();
        
        if (!isServer) return;
        
        _parameters = new Dictionary<Parameter, bool>();
        foreach (Parameter parameter in Enum.GetValues(typeof(Parameter)))
        {
            _parameters.Add(parameter, false);
        }
        
        Game.PauseGameEvent += OnPauseGame;
        Game.ContinueGameEvent += OnContinueGame;
        DeathEvent += OnDeath;
        if (Agent.isActiveAndEnabled) Agent.isStopped = Game.IsPaused;
    }

    private void OnDeath(object sender, EventArgsWithCharacter e)
    {
        Game.PauseGameEvent -= OnPauseGame;
        Game.ContinueGameEvent -= OnContinueGame;
        Game.Score += killScore;
    }

    private void OnContinueGame(object sender, EventArgs e)
    {
        if (Agent.isActiveAndEnabled) Agent.isStopped = false;
    }

    private void OnPauseGame(object sender, EventArgs e)
    {
        if (Agent.isActiveAndEnabled) Agent.isStopped = true;
    }
    

    private new void Update()
    {
        base.Update();

        if (!IsControllable()) return;

        if (_stateIndex < 0)
        {
            _stateIndex = 0;
            if (states.Length > 0) states[0].StartState();
        }
        
        foreach (Transition transition in transitions)
        {
            if (transition.from != _stateIndex) continue;

            bool allFulfilled = transition.conditions.All(
                condition => _parameters[condition.parameter] == condition.value);

            if (!allFulfilled) continue;
            
            states[_stateIndex].EndState();
            _stateIndex = transition.to;
            states[_stateIndex].StartState();
            break;
        }
        
        if (_stateIndex < states.Length) states[_stateIndex].UpdateState();
        
        float yRotation = transform.rotation.eulerAngles.y;
        transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }


    public void SetParameter(Parameter parameter, bool value)
    {
        _parameters[parameter] = value;
        ParameterChangeEvent?.Invoke(this, new EventArgsWithParameter(parameter, value));
    }
}
