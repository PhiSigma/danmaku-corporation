public class ScoreItem : PickupItem
{
    public float score = 50;
    
    protected override void Pickup(PlayerCharacter player)
    {
        Game.Score += score;
    }
}