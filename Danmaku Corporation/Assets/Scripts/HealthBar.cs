using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class HealthBar : MonoBehaviour
{
    private float maxValue = 1;
    private RectTransform _rectTransform;
    private Camera _camera;
    
    private RectTransform RectTransform
    {
        get
        {
            if (!_rectTransform) _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }

    private float _value;

    public float Value
    {
        get => _value;
        set
        {
            _value = value;
            RectTransform.offsetMax = new Vector2((_value - 1)*maxValue, 0);
        }
    }

    private void Start()
    {
        Value = 1;
        _camera = Camera.main;

        RectTransform parentRect = transform.parent.GetComponent<RectTransform>();
        if (parentRect) maxValue = parentRect.rect.width;
    }

    private void LateUpdate()
    {
        if (_camera)
        {
            transform.parent.transform.rotation = _camera.transform.rotation;
        }
    }
}