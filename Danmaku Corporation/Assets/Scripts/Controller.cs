﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Controller : NetworkBehaviour
{
    private string[] _axisNames;
    private float[] _axisValues;
    private Dictionary<string, int> _axisDictionary;

    public static Controller Instance;
    
    public bool IsReady { private set; get; }
    public bool HasGamepad { private set; get; }
    public Vector3 MousePosition { private set; get; }

    
    [Command]
    private void CmdSetup(string[] axisNames)
    {
        _axisDictionary = new Dictionary<string, int>();
        
        for (int i = 0; i < axisNames.Length; i++)
        {
            string axisName = axisNames[i];
            if (!_axisDictionary.ContainsKey(axisName)) _axisDictionary.Add(axisName, i);
        }
    }

    [Command]
    private void CmdSendValues(float[] axisValues, Vector3 mousePosition, bool hasGamepad)
    {
        _axisValues = axisValues;
        MousePosition = mousePosition;
        HasGamepad = hasGamepad;
        IsReady = true;
    }

    
    [Command]
    public void CmdSetPause(bool value)
    {
        Game.IsPaused = value;
    }

    [Command]
    public void CmdReload()
    {
        Game.Reload();
    }


    private void Start()
    {
        if (!isLocalPlayer) return;

        Instance = this;
        
        _axisNames = ControllerManager.GetAxisNames();
        _axisValues = new float[_axisNames.Length];
        
        CmdSetup(_axisNames);
    }

    private void Update()
    {
        if (!isLocalPlayer) return;

        for (int i = 0; i < _axisNames.Length; i++)
        {
            _axisValues[i] = Input.GetAxis(_axisNames[i]);
        }

        string[] joystickNames = Input.GetJoystickNames();
        HasGamepad = joystickNames.Length > 0 && joystickNames[0].Length > 0;
        CmdSendValues(_axisValues, Input.mousePosition, HasGamepad);
    }

    
    public float GetAxis(string axisName)
    {
        if (IsReady) return _axisValues[_axisDictionary[axisName]];
        
        Debug.LogWarning(isServer ? "Input data has not been sent yet." : "Input should be read from the server.");
        return 0;
    }
}
