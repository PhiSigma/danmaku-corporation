public class EnemyGoToAlarm : EnemyCanAttackNearby
{
    public float stoppingDistance = 3f;
    public float walkSpeed = 4.5f;

    public override void StartState()
    {
        MyEnemy.Agent.SetDestination(MyEnemy.TargetPosition);
        MyEnemy.Agent.stoppingDistance = stoppingDistance;
        MyEnemy.Agent.speed = walkSpeed;

        base.StartState();
    }
}