using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CinemachineTargetGroup))]
public class TargetGroupHelper : NetworkBehaviour
{
    private CinemachineTargetGroup _targetGroup;
    private int _playerCount;

    public Transform dummy;
    public float radius = 3.5f;
    public float soloRadius = 7;
    public float dummyRadius = 10;
    
    private void Start()
    {
        _targetGroup = GetComponent<CinemachineTargetGroup>();
        _playerCount = 
            ControllerManager.PlayerCharacters.Count(character => character && character.isActiveAndEnabled);
        RemakeTargetGroup();

        if (!isServer) return;
        foreach (PlayerCharacter character in ControllerManager.PlayerCharacters)
        {
            character.DeathEvent += OnTargetDeath;
        }
    }

    private void Update()
    {
        int playerCount =
            ControllerManager.PlayerCharacters.Count(character => character && character.isActiveAndEnabled);
        if (_playerCount != playerCount)
        {
            RemakeTargetGroup();
        }
    }

    private void RemakeTargetGroup()
    {
        int playerCount =
            ControllerManager.PlayerCharacters.Count(character => character && character.isActiveAndEnabled);
        
        CinemachineTargetGroup.Target[] targets =
            new CinemachineTargetGroup.Target[playerCount + 1];

        targets[0] = new CinemachineTargetGroup.Target
        {
            target = dummy.transform, radius = dummyRadius, weight = playerCount == 0 ? 1 : 0
        };

        int i = 1;
        foreach (PlayerCharacter character in ControllerManager.PlayerCharacters)
        {
            if (!character || !character.isActiveAndEnabled) continue;
            targets[i++] = new CinemachineTargetGroup.Target
            {
                target = character.transform, radius = playerCount > 1 ? radius : soloRadius, weight = 1
            };
        }

        _targetGroup.m_Targets = targets;
    }

    private void OnTargetDeath(object sender, EventArgsWithCharacter args)
    {
        Vector3 pos = args.Character.transform.position;
        SetDummy(pos);
        RpcSetDummy(pos);
    }

    [ClientRpc]
    private void RpcSetDummy(Vector3 pos)
    {
        SetDummy(pos);
    }
    
    private void SetDummy(Vector3 pos)
    {
        dummy.position = pos;
    }
}