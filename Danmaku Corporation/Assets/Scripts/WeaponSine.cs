using UnityEngine;

public class WeaponSine : WeaponBasic
{
    public float frequency = 1;
    public float ampFactor = 1;
    public float ampPower = 2;
    public float ampConst = 1;
    public Color secondaryColor = Color.yellow;
    
    public override void Fire(Vector3 position, Vector3 direction)
    {
        if (!(bulletPrefab is SineBullet))
        {
            Debug.LogError(name + " cannot handle " + bulletPrefab.name 
                           + ", should be " + nameof(SineBullet));
            return;
        }
        
        for (int i = 0; i < 2; i++)
        {
            Color c = i > 0 ? secondaryColor : color;
             
            BulletInfo info = new BulletInfo
            {
                StartPosition = position, Rotation = Quaternion.LookRotation(direction), Color = c,
                Radius = radius, Velocity = direction * speed, Duration = duration, Damage = damage
            };
            
            SineBulletInfo sineInfo = new SineBulletInfo
            {
                Frequency = frequency, Phi = i * 180, AmpPower = ampPower, AmpConst = ampConst, AmpFactor = ampFactor
            };

            SineBullet bullet = (SineBullet) ObjectPooler.Instance.GetPooledObject(bulletPrefab.gameObject.name);
            bullet.info = info;
            bullet.sineInfo = sineInfo;
            bullet.isActive = true;
         }
    }
}