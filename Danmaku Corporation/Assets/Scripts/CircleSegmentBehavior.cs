using UnityEngine.Networking;

public abstract class CircleSegmentBehavior : NetworkBehaviour
{
    public abstract float GetRange();
    public abstract float GetSubRange();
    public abstract float GetSpread();
}