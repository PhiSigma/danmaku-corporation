using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class InGameScoreDisplay : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnChangeScore))] private float _score = -1;
    private Text _text;

    private void Start()
    {
        _text = GetComponent<Text>();
        OnChangeScore(Game.Score);
    }

    private void Update()
    {
        if (isServer) OnChangeScore(Game.Score);
    }

    private void OnChangeScore(float value)
    {
        _score = value;
        _text.text = _score.ToString("F1");
    }
}