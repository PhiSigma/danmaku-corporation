using UnityEngine;
using UnityEngine.Networking;

public struct SpiralBulletInfo
{
    public float Frequency;
    public float AmpFactor;
    public float AmpPower;
}

public class SpiralBullet : Bullet
{
    [SyncVar] public SpiralBulletInfo spiralInfo;
    
    protected override void SetPosition(float t)
    {
        Vector3 normal = new Vector3(info.Velocity.z, info.Velocity.y, -info.Velocity.x).normalized;
        float r = Amplitude(t);
        transform.position = info.StartPosition +
                             r * (info.Velocity.normalized * Mathf.Cos(spiralInfo.Frequency * t) +
                                  normal * Mathf.Sin(spiralInfo.Frequency * t));
    }

    private float Amplitude(float t)
    {
        return spiralInfo.AmpFactor * Mathf.Pow(t, spiralInfo.AmpPower);
    }
}