using UnityEditor;
using UnityEngine;

public static class Reader
{
    [MenuItem("Assets/ReadAxes")]
    public static void ReadAxes()
    {
        ControllerManager instance = Object.FindObjectOfType<ControllerManager>();
        if (!instance)
        {
            Debug.LogError("Controller manager not found.");
            return;
        }
        
        Object inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];
        SerializedObject obj = new SerializedObject(inputManager);
        SerializedProperty axisArray = obj.FindProperty("m_Axes");

        int n = axisArray.arraySize;
        string[] axisNames = new string[n];
 
        for (int i = 0; i < axisArray.arraySize; i++)
        {
            SerializedProperty axis = axisArray.GetArrayElementAtIndex(i);
            axisNames[i] = axis.FindPropertyRelative("m_Name").stringValue;
        }

        instance.axisNames = axisNames;
    }
}