# Danmaku Corporation

## Story
Danmaku Corporation is a company specializing in security and combat technology, featuring autonomous robots that detect humans - even through walls! - and fire energy bullets at them. *Many* bullets. One day, the AI went rogue, attacking all humans in their vicinity and turning the company's life into a *bullet hell*, forcing them to evacuate and get two agents on the case.

You are these two agents, codenamed Scout and Doc, who must infiltrate the robot-infested company and defeat the leader of each floor's group of rogue AI in order to regain control.

## Agents
### Scout
Scout wears a special visor, recognizable by the glowing red stripe, which, when focusing, allows her to see the detection ranges of enemies, as well as the healing range of her partner Doc. She cannot take many hits, so she likes to stay just outside the enemy's range.
### Doc
Doc, with her trademark medical bag, is not only an agent, but also a doctor. Her special ability is to heal any human in her close vicinity when focusing. Even though she cannot heal herself, she has taken good care of her health, allowing her to take more enemy hits than Scout.

## Enemies
Other than the standard enemy robot, which you meet at the very beginning of your mission, there are a few special types of enemies.
### Scaredy-Cat
If the *scaredy-cat* sees you, it is scared and rather than shoot you, it runs off - straight for the next *alarm button*. Only after pressing the alarm does it dare to attack you. However, take care - some scaredy-cats have powerful weapons, and even if not, the alarm can be dangerous.
### Alarm Button
Can a button be considered an enemy, even if it cannot shoot or be destroyed? In any case, if an agent or *scaredy-cat* steps on it, this button draws all enemies in its vicinity towards it. If there are many enemies around, this can be quite deadly...
### Guard Turret
The *guard turret* cannot change position, but it can still turn around to face and shoot you. It tends to have a very large detection range.
### Security Camera
Other than most enemies, the *security camera* only detects players in front of it. When it does so, it triggers an alarm. At least it cannot shoot you, but it also cannot be destroyed...
### Boss
Each floor has a *boss* in charge of the rogue robots, which must be defeated. The boss of the prototype floor is MARTIN. He has very powerful weapons, can take a huge amount of hits and once he detects you, he will never let go of you until you die. Do not get close to him until you are ready!

## Items
Defeated enemies randomly drop items, and some items are also left on the company's floor for you to pick up.
### Healing Energy
The green spheres full of healing energy give you back some health.
### Score Points
The golden spheres contain energy that is useful to your employer, thus increasing your performance rating.
### Weapons
The cylinders give you a new weapon with slightly randomized properties. The two weapons on pedestals may be stronger than those dropped by enemies.

## Default Controls
### Keyboard and Mouse
**Movement**: WASD  
**Aiming**: Mouse movement  
**Shooting**: hold Space  
**Focusing**: hold Left Shift  
**Pause**: ESC  
**Switch Weapon**: Q, E  
### XBox360 Controller
**Movement**: Left Stick  
**Aiming**: Right Stick  
**Shooting**: hold RB  
**Focusing**: hold LB  
**Pause**: Start  
**Switch Weapon**: LT, RT